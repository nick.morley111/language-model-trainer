# Open Playground / Language model trainer

Quickly and easily train a language model to process data and give desired results.

## Purpose 

If you need to carry out a repetitive task on textual data, such as summarising or extracting specific information from text, you want to be able to quickly train a computer to carry out that task so you can get more done.

This tool helps by making it easy to train a language model to perform a task and to run it on your data unlike existing approaches that lack the infrastructure to connect to my data and/or require prohibitive amounts time/expertise to setup and deploy.

## Installation

```bash
npm install
```

```bash
npm start
```